" Vim color file
" Author: Carolina de Senne Garcia <desennecarol@gmail.com>

hi clear

set background=dark
if exists("syntax_on")
   syntax reset
endif
set syntax=java
let g:colors_name="javaCarol"

"
" Support for 256-color terminal
"
if &t_Co > 255
   hi Normal ctermfg=15 ctermbg=0
   hi CursorLine ctermbg=0 cterm=none
   hi CursorLineNr ctermfg=208 cterm=none
   hi Boolean ctermfg=13
   hi Character ctermfg=13
   hi Number ctermfg=13
   hi String ctermfg=13
   hi Conditional ctermfg=208 cterm=bold
   hi Constant ctermfg=135 cterm=bold
   hi Cursor ctermfg=0 ctermbg=0
   hi Debug ctermfg=225 cterm=bold
   hi Define ctermfg=208
   hi Delimiter ctermfg=15

   hi DiffAdd ctermbg=24
   hi DiffChange ctermfg=181 ctermbg=239
   hi DiffDelete ctermfg=162 ctermbg=53
   hi DiffText ctermbg=102 cterm=bold

   hi Directory ctermfg=118 cterm=bold
   hi Error ctermfg=219 ctermbg=89
   hi ErrorMsg ctermfg=199 ctermbg=16 cterm=bold
   hi Exception ctermfg=118 cterm=bold
   hi Float ctermfg=135
   hi FoldColumn ctermfg=67 ctermbg=16
   hi Folded ctermfg=67 ctermbg=16
   hi Function ctermfg=118
   hi Identifier ctermfg=208 cterm=none
   hi Ignore ctermfg=244 ctermbg=232
   hi IncSearch ctermfg=193 ctermbg=16

   hi keyword ctermfg=161 cterm=bold
   hi Label ctermfg=229 cterm=none
   hi Macro ctermfg=193
   hi SpecialKey ctermfg=208

   hi MatchParen ctermfg=208 ctermbg=233 cterm=bold
   hi ModeMsg ctermfg=229
   hi MoreMsg ctermfg=229
   hi Operator ctermfg=208

" complete menu
   hi Pmenu ctermfg=21 ctermbg=16
   hi PmenuSel ctermfg=21 ctermbg=244
   hi PmenuSbar ctermbg=232
   hi PmenuThumb ctermfg=21

   hi PreCondit ctermfg=208 cterm=bold
   hi PreProc ctermfg=208
   hi Question ctermfg=160
   hi Repeat ctermfg=208 cterm=bold
   hi Search ctermfg=253 ctermbg=66

" marks column
   hi SignColumn ctermfg=15 ctermbg=235
   hi SpecialChar ctermfg=15 cterm=bold
   hi SpecialComment ctermfg=15 cterm=bold
   hi Special ctermfg=15
   if has("spell")
       hi SpellBad ctermbg=52
       hi SpellCap ctermbg=17
       hi SpellLocal ctermbg=17
       hi SpellRare ctermfg=none ctermbg=none cterm=reverse
   endif
   hi Statement ctermfg=13 cterm=bold
   hi StatusLine ctermfg=238 ctermbg=253
   hi StatusLineNC ctermfg=244 ctermbg=232
   hi StorageClass ctermfg=208
   hi Structure ctermfg=57
   hi Tag ctermfg=161
   hi Title ctermfg=166
   hi Todo ctermfg=45 ctermbg=232 cterm=bold

   hi Typedef ctermfg=57
   hi Type ctermfg=57 cterm=bold
   hi Underlined ctermfg=244 cterm=underline

   hi VertSplit ctermfg=244 ctermbg=232 cterm=bold
   hi VisualNOS ctermbg=238
   "hi Visual ctermbg=235
   hi Visual ctermbg=Gray
   hi WarningMsg ctermfg=160 ctermbg=238 cterm=bold
   hi WildMenu ctermfg=15 ctermbg=16

   hi Comment ctermfg=45 cterm=none
   hi CursorColumn ctermbg=0
   hi ColorColumn ctermbg=0
   hi LineNr ctermfg=208 ctermbg=0
   hi NonText ctermfg=0

   hi SpecialKey ctermfg=15

   end

